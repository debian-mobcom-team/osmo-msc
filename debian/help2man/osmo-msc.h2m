[DESCRIPTION]
OsmoMSC is the Osmocom implementation of a Mobile Switching Center (MSC).
It implements the following interfaces:

    3GPP AoIP over M3UA or SUA towards BSCs, for example OsmoBSC, possibly via a STP like OsmoSTP
    3GPP IuCS over M3UA or SUA towards RNCs or HNBGWs, for example OsmoHNBGW, possibly via a STP like OsmoSTP
    Osmocom GSUP (alternative to SS7/TACP/MAP) towards an HLR such as OsmoHLR
        Subscriber management
        Supplementary Services (SS/USSD)
        Short Message Service (SMS)
        inter-MSC handover
    SMPP v3.4 for external SMS entities (minimal SMSC is built-in)
    MGCP for controlling an external Media Gateway like OsmoMGW
    MNCC for external call-control handlers, such as osmo-sip-connector for SIP trunks
    Osmocom VTY interface for configuration + introspection
    Osmocom CTRL interface for programmatic access to internal state/configuration


.UR "https://osmocom.org/projects/osmomsc/wiki"
.UE

[SEE ALSO]
.UR "http://ftp.osmocom.org/docs/latest/osmomsc-usermanual.pdf"
.UE
.P
.UR "http://ftp.osmocom.org/docs/latest/osmomsc-vty-reference.pdf"
.UE
